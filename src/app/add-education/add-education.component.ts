import {Component, OnInit} from '@angular/core';
import {EmployerService} from "../services/employer/employer.service";
import {EducationService} from "../services/education/education.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Education} from "../services/education/education";
import {Employer} from "../services/employer/employer";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-education',
  templateUrl: './add-education.component.html',
  styleUrls: ['./add-education.component.css']
})
export class AddEducationComponent implements OnInit {

  education: Education = new Education();
  employer: Employer;
  employerId: number;
  startDate;
  endDate;
  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private employerService: EmployerService,
              private educationService: EducationService,
              private route: ActivatedRoute,
              private router: Router,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.education.startDate = new Date(this.startDate.year, this.startDate.month, this.startDate.day);
    this.education.endDate = new Date(this.endDate.year, this.endDate.month, this.endDate.day);

    this.educationService.createEducation(this.education, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.errorList = new Array<string>();
        this.education = new Education();
        this.parameters = new Map<string, string>();
      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        this.education = new Education();
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      });
  }

  goToNext() {
    this.router.navigate(['addSkill', this.employer.id]);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }

}
