import {Component, OnInit} from '@angular/core';
import {EmployerService} from "../services/employer/employer.service";
import {Employer} from "../services/employer/employer";
import {ActivatedRoute, Router} from "@angular/router";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-edit-employer',
  templateUrl: './edit-employer.component.html',
  styleUrls: ['./edit-employer.component.css']
})
export class EditEmployerComponent implements OnInit {

  employer: Employer;
  birthDate;
  hiredate;

  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private employerService: EmployerService,
              private router: Router,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
    this.getEmployer();
  }

  saveEmployer(employerEdited: Employer): void {
    //this.employer.birthDate = new Date(this.birthDate.year, this.birthDate.month, this.birthDate.day);
    //this.employer.hiredate = new Date(this.hiredate.year, this.hiredate.month, this.hiredate.day);

    this.employerService.updateEmployer(this.employer.id, this.employer)
      .subscribe(data => {
          this.router.navigate(['employer']);
          console.log(data);
        },
        error => {
          console.log(error);
          this.errorList = this.addErrorService.addErrorsToList(error);
          this.parameters = this.addErrorService.addErrorsToMap(error);
          document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        });

  } 

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }

}
