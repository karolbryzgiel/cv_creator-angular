import {Component, OnInit, Input} from '@angular/core';
import {EmployerService} from '../services/employer/employer.service';
import {Employer} from '../services/employer/employer';

import {Experience} from '../services/experience/experience';

import { ActivatedRoute } from '@angular/router';
import {ExperienceService} from '../services/experience/experience.service';
import {Observable} from 'rxjs';
import {Skills} from "../services/skills/skills";
import {SkillsService} from "../services/skills/skills.service";
import {EducationService} from "../services/education/education.service";
import {Education} from "../services/education/education";
import {HobbyService} from "../services/hobby/hobby.service";
import {Hobbies} from "../services/hobby/hobby";
import {UrlLinksService} from "../services/urlLinks/url-links.service";
import {urlLinks} from "../services/urlLinks/urlLinks";
import {CertificationService} from "../services/certification/certification.service";
import {Certification} from "../services/certification/certification";
import {ClientService} from "../services/client/client.service";
import {ProjectsService} from "../services/projects/projects.service";
import {EmployerProjectsService} from "../services/employer_projects/employer-projects.service";
import {EmployerProjects} from "../services/employer_projects/employer-projects";
import {Projects} from "../services/projects/projects";
import {Client} from "../services/client/client";

@Component({
  selector: 'employer-details',
  templateUrl: './employer-details.component.html',
  styleUrls: ['./employer-details.component.css']
})

export class EmployerDetailsComponent implements OnInit {

  @Input()
  experience: Experience;
  experiences: Observable<Experience[]>;
  employer: Employer;
  skill: Skills;
  skills: Observable<Skills[]>;
  education: Education;
  educations: Observable<Education[]>;
  hobby: Hobbies;
  hobbies: Observable<Hobbies[]>;
  urlLink: urlLinks;
  urlLinks: Observable<urlLinks[]>;
  certification: Certification;
  certifications: Observable<Certification[]>;
  employerProject: EmployerProjects;
  employerProjects: Observable<EmployerProjects[]>;
  project: Projects;
  projects: Observable<Projects[]>;

  constructor(private route: ActivatedRoute,
              private experienceService: ExperienceService,
              private employerService: EmployerService,
              private skillsService: SkillsService,
              private educationService: EducationService,
              private hobbyService: HobbyService,
              private urlLinksService: UrlLinksService,
              private certificationService: CertificationService,
              private employerProjectsService: EmployerProjectsService,
              private clientService: ClientService,
              private projectsService: ProjectsService) {
  }


  ngOnInit(): void {
    this.getEmployer();
    this.getExperience();
    this.getSkills();
    this.getEducations();
    this.getHobbies();
    this.getUrlLinks();
    this.getCertificationList();
    this.getEmployerProjects();
  }

  getCertificationList(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.certifications = this.certificationService.getCertificationList(id);
  }
  getUrlLinks(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.urlLinks = this.urlLinksService.getUrlLinksList(id);
  }

  getHobbies(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.hobbies = this.hobbyService.getHobbyList(id);
  }

  getEducations(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.educations = this.educationService.getEducationList(id);
  }
  getSkills(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.skills = this.skillsService.getSkillsList(id);
  }

  getExperience(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    //this.experienceService.getExperienceList(id).subscribe(experience => this.experience = experience);
    this.experiences = this.experienceService.getExperienceList(id);
  }
  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }

  getEmployerProjects(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerProjects = this.employerProjectsService.getEmployerProjectsList(id);
  }



}

