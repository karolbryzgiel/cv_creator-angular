import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUrlLinksComponent } from './add-url-links.component';

describe('AddUrlLinksComponent', () => {
  let component: AddUrlLinksComponent;
  let fixture: ComponentFixture<AddUrlLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUrlLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUrlLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
