import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employer} from "../services/employer/employer";
import {EmployerService} from "../services/employer/employer.service";
import {SkillsService} from "../services/skills/skills.service";
import {Skills} from "../services/skills/skills";
import {urlLinks} from "../services/urlLinks/urlLinks";
import {UrlLinksService} from "../services/urlLinks/url-links.service";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-url-links',
  templateUrl: './add-url-links.component.html',
  styleUrls: ['./add-url-links.component.css']
})
export class AddUrlLinksComponent implements OnInit {

  urlLinks: urlLinks = new urlLinks();
  employer: Employer;
  employerId: number
  error: string;

  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employerService: EmployerService,
              private urlLinksService: UrlLinksService,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.urlLinksService.createUrlLink(this.urlLinks, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.errorList = new Array<string>();
        this.urlLinks = new urlLinks();
      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        this.urlLinks = new urlLinks();
      });
  }

  goToNext() {
    this.router.navigate(['addCertification', this.employer.id]);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }
}
