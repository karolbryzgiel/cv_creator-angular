interface ErrorJson {
  error: string;
  errorCode: string;
  errorMessage: string;
  errorMap: Map<string, any>;
  errorList: Array<any>;
  parameters: { [name: string]: string };
}
