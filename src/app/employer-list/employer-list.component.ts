import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

import {EmployerService} from '../services/employer/employer.service';
import {Employer} from '../services/employer/employer';

@Component({
  selector: 'employer-list',
  templateUrl: './employer-list.component.html',
  styleUrls: ['./employer-list.component.css']
})
export class EmployerListComponent implements OnInit {

  @Input() employer: Employer;
  employers: Observable<Employer[]>;

  constructor(private employerService: EmployerService) {
  }

  ngOnInit() {
    this.reloadData();
  }


  reloadData() {
    this.employers = this.employerService.getEmployersList();
  }

  public delete(id: number) {
    this.employerService.deleteEmployer(id)
      .subscribe(
        data => {
          console.log(data);
          this.employers = this.employerService.getEmployersList();
        },
        error => console.log(error));
  }
/*
  deleteEmployer() {
    this.employerService.deleteEmployer(this.employer.id)
      .subscribe(
        data => {
          console.log(data);
          this.employers = this.employerService.getEmployersList();
        },
        error => console.log(error));
  }
  */
}
