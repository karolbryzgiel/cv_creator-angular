import {Component, Input, OnInit} from '@angular/core';
import {Experience} from "../services/experience/experience";
import {ExperienceService} from "../services/experience/experience.service";
import {Employer} from "../services/employer/employer";
import {EmployerService} from "../services/employer/employer.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {Certification} from "../services/certification/certification";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-experience',
  templateUrl: './add-experience.component.html',
  styleUrls: ['./add-experience.component.css']
})
export class AddExperienceComponent implements OnInit {

  experience: Experience = new Experience();
  employer: Employer;
  employerId: number;
  startDate;
  endDate;
  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;


  constructor(private route: ActivatedRoute,
              private router: Router,
              private experienceService: ExperienceService,
              private employerService: EmployerService,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.experience.startDate = new Date(this.startDate.year, this.startDate.month, this.startDate.day);
    this.experience.endDate = new Date(this.endDate.year, this.endDate.month, this.endDate.day);

    this.experienceService.createExperience(this.experience, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.experience = new Experience();
        this.parameters = new Map<string, string>();
        this.errorList = new Array<string>();

      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        this.experience = new Experience();
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      });
  }

  goToNext() {
    this.router.navigate(['addEducation', this.employer.id]);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }


}
