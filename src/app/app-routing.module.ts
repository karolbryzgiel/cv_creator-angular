import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { CreateEmployerComponent } from './create-employer/create-employer.component';
import {EmployerDetailsComponent} from './employer-details/employer-details.component';
import {EditEmployerComponent} from "./edit-employer/edit-employer.component";
import {AddExperienceComponent} from "./add-experience/add-experience.component";
import {AddEducationComponent} from "./add-education/add-education.component";
import {AddSkillsComponent} from "./add-skills/add-skills.component";
import {AddHobbiesComponent} from "./add-hobbies/add-hobbies.component";
import {AddUrlLinksComponent} from "./add-url-links/add-url-links.component";
import {AddCertificationsComponent} from "./add-certifications/add-certifications.component";

const routes: Routes = [
  { path: '', redirectTo: 'employer', pathMatch: 'full' },
  { path: 'employer', component: EmployerListComponent },
  { path: 'add', component: CreateEmployerComponent },
  { path: 'employer/detail/:id', component: EmployerDetailsComponent},
  { path: 'employer/edit/:id', component: EditEmployerComponent},
  { path: 'addExperience/:id', component: AddExperienceComponent},
  { path: 'addEducation/:id', component: AddEducationComponent},
  { path: 'addSkill/:id', component: AddSkillsComponent},
  { path: 'addHobby/:id', component: AddHobbiesComponent},
  { path: 'addUrlLink/:id', component: AddUrlLinksComponent},
  { path: 'addCertification/:id', component: AddCertificationsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
