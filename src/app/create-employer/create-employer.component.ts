import {Component, OnInit, Pipe} from '@angular/core';

import {Employer} from '../services/employer/employer';
import {EmployerService} from '../services/employer/employer.service';
import {Router} from "@angular/router";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'create-employer',
  templateUrl: './create-employer.component.html',
  styleUrls: ['./create-employer.component.css']
})

export class CreateEmployerComponent implements OnInit {

  employer: Employer = new Employer();
  birthDate;
  hiredate;

  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private employerService: EmployerService,
              private router: Router,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
  }

  save() {
    this.employer.birthDate = new Date(this.birthDate.year, this.birthDate.month, this.birthDate.day);
    this.employer.hiredate = new Date(this.hiredate.year, this.hiredate.month, this.hiredate.day);

    this.employerService.createEmployer(this.employer)
      .subscribe(data => {
          this.router.navigate(['addExperience', JSON.parse(data['id'])]);
          this.employer = new Employer();
        },
        error => {
          this.errorList = this.addErrorService.addErrorsToList(error);
          this.parameters = this.addErrorService.addErrorsToMap(error);
          document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        });
  }


}
