import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employer} from "../services/employer/employer";
import {EmployerService} from "../services/employer/employer.service";
import {Experience} from "../services/experience/experience";
import {ExperienceService} from "../services/experience/experience.service";
import {Hobbies} from "../services/hobby/hobby";
import {HobbyService} from "../services/hobby/hobby.service";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-hobbies',
  templateUrl: './add-hobbies.component.html',
  styleUrls: ['./add-hobbies.component.css']
})
export class AddHobbiesComponent implements OnInit {

  hobby: Hobbies = new Hobbies();
  employer: Employer;
  employerId: number;
  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private hobbyService: HobbyService,
              private employerService: EmployerService,
              private addErrorService: AddErrorsService) {
  }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.hobbyService.createHobby(this.hobby, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.errorList = new Array<string>();
        this.hobby = new Hobbies();
        this.parameters = new Map<string, string>();
      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        this.hobby = new Hobbies();
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      });
  }

  goToNext() {
    this.router.navigate(['addUrlLink', this.employer.id]);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }
}
