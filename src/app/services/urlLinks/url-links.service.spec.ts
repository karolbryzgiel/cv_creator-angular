import { TestBed, inject } from '@angular/core/testing';

import { UrlLinksService } from './url-links.service';

describe('UrlLinksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrlLinksService]
    });
  });

  it('should be created', inject([UrlLinksService], (service: UrlLinksService) => {
    expect(service).toBeTruthy();
  }));
});
