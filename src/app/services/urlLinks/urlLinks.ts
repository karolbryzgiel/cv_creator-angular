import {Employer} from "../employer/employer";

export class urlLinks {
  id: number;
  linkName: string;
  url: string;
  employerByEmployerId: Employer;

}
