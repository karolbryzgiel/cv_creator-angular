import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class UrlLinksService {

  private baseUrl: 'http://localhost:8080/employers/'

  constructor(private http: HttpClient) {
  }


  getUrlLinksList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employers/'+`${employerId}` + `/urlLinks`);
    //return this.http.get('http://localhost:8080/employers/' + `${employerId}` + `/urlLinks`);
    //return this.http.get(`${this.baseUrl}` + `/1/skills`); - nie dziala
    //return this.http.get('http://localhost:8080/employers/1/urlLinks');
  }

  createUrlLink(urlLink: Object, employerId: number): Observable<Object> {
      return this.http.post('http://localhost:8080/employers/'+`${employerId}` + `/urlLinks`, urlLink);
  }

}
