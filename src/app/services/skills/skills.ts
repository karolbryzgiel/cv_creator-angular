import {Employer} from "../employer/employer";

export class Skills {
  id: number;
  name: string;
  level: string;
  employerByEmployerId: Employer;
}
