import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  private baseUrl: 'http://localhost:8080/employers';

  constructor(private http: HttpClient) { }

  createExperience(experience: Object, employerId: number): Observable<Object> {
    return this.http.post('http://localhost:8080/employers/'+`${employerId}` + `/experience`, experience);
  }

  updateExperience(employerId: number, experienceId: number, experience: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}` + `${employerId}` + `/experience` + `/${experienceId}`, experience);
  }

  deleteExperience(employerId: number, experienceId: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `${employerId}` + `/experience` + `/${experienceId}`);
  }

  getExperienceList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employers/'+`${employerId}` + `/experience`);
    //return this.http.get(`${this.baseUrl}` + `/1/experience`); - nie dziala
    //return this.http.get(`${this.baseUrl}/${employerId}/experience`);
  }

  getExperienceById(employerId: number, experienceId: number): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `${employerId}` + `/experience` + `/${experienceId}`);
  }
}
