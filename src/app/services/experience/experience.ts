import {Employer} from '../employer/employer';

export class Experience {
  id: number;
  startDate: Date;
  endDate: Date;
  companyName: string;
  position: string;
  employerByEmployerId: Employer;
}
