import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class HobbyService {

  constructor(private http: HttpClient) {
  }

  getHobbyList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employers/'+`${employerId}` + `/hobbies`);
    //return this.http.get('http://localhost:8080/employers/1/hobbies');
  }

  createHobby(hobby: Object, employerId: number): Observable<Object> {
    return this.http.post('http://localhost:8080/employers/'+`${employerId}` + `/hobbies`, hobby);
  }

}
