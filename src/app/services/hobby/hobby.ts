import {Employer} from "../employer/employer";

export class Hobbies {
  category: string;
  name: string;
  employerByEmployerId: Employer;
}
