import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, tap} from "rxjs/operators";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable({
  providedIn: 'root'
})

export class EmployerService {

  private baseUrl = 'http://localhost:8080/employer';

  constructor(private http: HttpClient) {
  }

  createEmployer(employer: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}/`, employer);
  }

  updateEmployer(id: number, employer: Object): Observable<Object> {
    return this.http.put(`http://localhost:8080/employer/` + `${id}`, employer);
  }

  deleteEmployer(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  getEmployersList(): Observable<any> {
    return this.http.get(`${this.baseUrl}` + `s`);
  }

  getEmployersById(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }
}
