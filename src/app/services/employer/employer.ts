export class Employer {
  id: number;
  pesel: number;
  name: string;
  surname: string;
  birthDate: Date;
  address: string;
  phoneNumber: number;
  emailAddress: string;
  hiredate: Date;
}
