import {Employer} from "../employer/employer";

export class Projects {
  id: number;
  description: string;
  end_date: Date;
  start_date: Date;
  project_name: String;
  
}
