import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private http: HttpClient) { }

  getProjectsList(clientId: number): Observable<any> {
    return this.http.get('http://localhost:8080/clients/'+`${clientId}` + `/projects`);
    //return this.http.get(`${this.baseUrl}` + `/1/experience`); - nie dziala
    //return this.http.get(`${this.baseUrl}/${employerId}/experience`);
  }

}
