import { TestBed, inject } from '@angular/core/testing';

import { EmployerProjectsService } from './employer-projects.service';

describe('EmployerProjectsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployerProjectsService]
    });
  });

  it('should be created', inject([EmployerProjectsService], (service: EmployerProjectsService) => {
    expect(service).toBeTruthy();
  }));
});
