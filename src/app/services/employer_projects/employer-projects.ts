import {Employer} from "../employer/employer";
import {Projects} from "../projects/projects";

export class EmployerProjects {
  id: number;
  function: string;
  endDate: Date;
  startDate: Date;
  employerByEmployerId: Employer;
  projectsByProjectsId: Projects;

}
