import { Injectable } from '@angular/core';
import {Observable} from "rxjs/index";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployerProjectsService {

  constructor(private http: HttpClient) { }

  getEmployerProjectsList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employer/'+`${employerId}` + `/projects`);
  }

}
