import {Employer} from "../employer/employer";

export class Education {
  id: number;
  nameOfOrganisation: string;
  direction: string;
  startDate: Date;
  specialization: string;
  professionalTitle: string;
  endDate: Date;
  employerByEmployerId: Employer;
}
