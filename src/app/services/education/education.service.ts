import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class EducationService {

  constructor(private http: HttpClient) { }

  getEducationList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employers/'+`${employerId}` + `/education`);
    //return this.http.get('http://localhost:8080/employers/1/education');
    //return this.http.get(`${this.baseUrl}` + `/1/skills`); - nie dziala
    //return this.http.get(`${this.baseUrl}/${employerId}/skills`);
  }

  createEducation(education: Object, employerId: number): Observable<Object> {
    return this.http.post('http://localhost:8080/employers/'+`${employerId}` + `/education`, education);
  }

}
