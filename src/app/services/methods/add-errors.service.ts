import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddErrorsService {

  constructor() {
  }

  errorList: Array<string>;
  parameters: Map<string, string>;

  convertJson(error: any): ErrorJson {
    const myObjStr = JSON.stringify(error);
    let obj: ErrorJson = JSON.parse(myObjStr);
    const errorMessage = JSON.stringify(obj.error);
    let object: ErrorJson = JSON.parse(errorMessage);
    return object;
  }

  addErrorsToList(error: any): Array<string> {
    this.errorList = new Array<string>();
    let errors: ErrorJson = this.convertJson(error);

    this.errorList = errors.errorList;
    return this.errorList;
  }

  addErrorsToMap(error: any): Map<string, string> {
    this.parameters = new Map<string, string>();
    let errors: ErrorJson = this.convertJson(error);

    Object.keys(errors.errorMap).forEach(key => {
      this.addParameter(key, errors.errorMap[key]);
    });
    return this.parameters;
  }

  addParameter(key: string, value: string) {
    this.parameters.set(key, value);
  }

  getParameters() {
    return this.parameters;
  }

}
