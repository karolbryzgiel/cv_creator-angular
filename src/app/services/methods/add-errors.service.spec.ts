import { TestBed, inject } from '@angular/core/testing';

import { AddErrorsService } from './add-errors.service';

describe('AddErrorsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddErrorsService]
    });
  });

  it('should be created', inject([AddErrorsService], (service: AddErrorsService) => {
    expect(service).toBeTruthy();
  }));
});
