import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class CertificationService {

  constructor(private http: HttpClient) {
  }

  getCertificationList(employerId: number): Observable<any> {
    return this.http.get('http://localhost:8080/employers/' + `${employerId}` + `/certification`);

  }

  createCertification(certification: Object, employerId: number): Observable<Object> {
    return this.http.post('http://localhost:8080/employers/'+`${employerId}` + `/certification`, certification);
  }

}
