import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EmployerService} from "../services/employer/employer.service";
import {SkillsService} from "../services/skills/skills.service";
import {Skills} from "../services/skills/skills";
import {Employer} from "../services/employer/employer";
import {Hobbies} from "../services/hobby/hobby";
import {Experience} from "../services/experience/experience";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-skills',
  templateUrl: './add-skills.component.html',
  styleUrls: ['./add-skills.component.css']
})
export class AddSkillsComponent implements OnInit {

  skills: Skills = new Skills();
  employer: Employer;
  employerId: number;
  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employerService: EmployerService,
              private skillsService: SkillsService,
              private addErrorService: AddErrorsService) { }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.skillsService.createSkills(this.skills, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.errorList = new Array<string>();
        this.skills = new Skills()
        this.parameters = new Map<string, string>();

      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        this.skills = new Skills()
      });
  }

  goToNext() {
    this.router.navigate(['addHobby', this.employer.id]);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }
}
