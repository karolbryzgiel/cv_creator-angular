import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CreateEmployerComponent } from './create-employer/create-employer.component';
import { EmployerDetailsComponent } from './employer-details/employer-details.component';
import { EmployerListComponent } from './employer-list/employer-list.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EditEmployerComponent } from './edit-employer/edit-employer.component';
import { AddExperienceComponent } from './add-experience/add-experience.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AddEducationComponent } from './add-education/add-education.component';
import { AddSkillsComponent } from './add-skills/add-skills.component';
import { AddHobbiesComponent } from './add-hobbies/add-hobbies.component';
import { AddUrlLinksComponent } from './add-url-links/add-url-links.component';
import { AddCertificationsComponent } from './add-certifications/add-certifications.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {GetValuesPipe} from "./services/pipes/get-values.pipe";

@NgModule({
  declarations: [
    AppComponent,
    CreateEmployerComponent,
    EmployerDetailsComponent,
    EmployerListComponent,
    EditEmployerComponent,
    AddExperienceComponent,
    AddEducationComponent,
    AddSkillsComponent,
    AddHobbiesComponent,
    AddUrlLinksComponent,
    AddCertificationsComponent,
    GetValuesPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    NgbModule,
    MatDatepickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
