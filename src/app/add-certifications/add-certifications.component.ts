import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employer} from "../services/employer/employer";
import {EmployerService} from "../services/employer/employer.service";
import {urlLinks} from "../services/urlLinks/urlLinks";
import {UrlLinksService} from "../services/urlLinks/url-links.service";
import {Certification} from "../services/certification/certification";
import {CertificationService} from "../services/certification/certification.service";
import {Education} from "../services/education/education";
import {CreateEmployerComponent} from "../create-employer/create-employer.component";
import {AddErrorsService} from "../services/methods/add-errors.service";

@Component({
  selector: 'app-add-certifications',
  templateUrl: './add-certifications.component.html',
  styleUrls: ['./add-certifications.component.css']
})
export class AddCertificationsComponent implements OnInit {


  certification: Certification = new Certification();
  employer: Employer
  employerId: number;
  dateOfGet;
  error: string;
  errorList: Array<string>;
  parameters: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private employerService: EmployerService,
              private certificationService: CertificationService,
              private addErrorService: AddErrorsService) { }

  ngOnInit() {
    this.getEmployer();
  }

  save() {
    this.certification.dateofGet = new Date(this.dateOfGet.year, this.dateOfGet.month, this.dateOfGet.day);
    this.certificationService.createCertification(this.certification, this.employer.id)
      .subscribe(data => {
        this.employerId = JSON.parse(data['employerByEmployerId']['id']);
        this.errorList = new Array<string>();
        this.certification = new Certification();
        this.parameters = new Map<string, string>();
      }, error => {
        this.errorList = this.addErrorService.addErrorsToList(error);
        this.parameters = this.addErrorService.addErrorsToMap(error);
        this.certification = new Certification();
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      });
  }


  goToNext() {
    this.router.navigate(['employer']);
  }

  getEmployer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employerService.getEmployersById(id).subscribe(employer => this.employer = employer);
  }
}
